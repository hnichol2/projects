#!/usr/bin/env python2.7

import logging
import os
import socket
import sqlite3
import sys

import tornado.ioloop
import tornado.options
import tornado.web

# Constants

PORT     = 9888
DB_PATH  = 'teams.db'
CSV_PATH = 'teams.csv'

# Database

class Database(object):
    def __init__(self,data=CSV_PATH, path=DB_PATH):
        # TODO: Connect to database
        self.logger = logging.getLogger()
        self.data   = data
        self.path   = path
        self.conn   = sqlite3.connect(DB_PATH)

        # TODO: Create table and load CSV data
        self._create_tables()
        self.load_tables()

    def _create_tables(self):
        with self.conn:
            cursor = self.conn.cursor()
            teams_sql = '''
            CREATE Table IF NOT EXISTS Teams(
                Name    TEXT    NOT NULL,
                Image   TEXT    NOT NULL,
            )'''
            cursor.execute(teams_sql)
            self.logger.info('Created Teams Table')

    def _load_tables(self):
        with self.conn:
            cursor = self.conn.cursor()
            teams_sql = 'INSERT OR REPLACE INTO Teams(Name, Image) VALUES (?, ?)'
            cursor.execute(teams_sql, (team['name'], image['image']))


    def search(self, query):
        # TODO: Return teams whose Name is similar to query
        with self.conn:
            cursor  = self.conn.cursor()
            teams_sql ='''
            SELECT Teams.Name, Image
            FROM Teams
            WHERE Name LIKE (?)
            '''
            return cursor.execute(teams_sql, (query,))

# Handlers

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        # TODO: Get query value from form
        title = self.get_argument('name', '')
        # TODO: Search for teams if query is valid
        limit = int(self.get_argument('limit', 0))
        teams = self.application.database.teams('%{}%'.format(name), limit)
        # TODO: Render 'teams.html' template by passing search results
        self.render('teams.html', name=name, teams=teams)
# Application

class Application(tornado.web.Application):
    def __init__(self, port=PORT):
        tornado.web.Application.__init__(self, debug=True)
        self.logger   = logging.getLogger()
        self.ioloop   = tornado.ioloop.IOLoop.instance()
        # TODO: Define database and port instance variables
        self.database = Database()
        self.port     = port
                # TODO: Add IndexHandler
        self.add_handlers('', [
            (r'/'       , IndexHandler)
        ])


    def run(self):
        try:
            self.listen(self.port)
        except socket.error as e:
            self.logger.fatal('Unable to listen on {} = {}'.format(self.port, e))
            sys.exit(1)

        self.ioloop.start()

# Main Execution

if __name__ == '__main__':
    tornado.options.parse_command_line()

    application = Application()
    application.run()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
