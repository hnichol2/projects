''' playit.database - SQLite Database '''

import logging
import sqlite3
import yaml

# Database

class Database(object):
    SQLITE_PATH = 'playit.db'
    YAML_PATH   = 'assets/yaml/playit.yaml'

    def __init__(self, data=YAML_PATH, path=SQLITE_PATH):
        self.logger = logging.getLogger()
        self.data   = data
        self.path   = path
        self.conn   = sqlite3.connect(self.path)

        self._create_tables()
        self._load_tables()

    def _create_tables(self):
        environments_sql = '''
        CREATE TABLE IF NOT EXISTS Environments (
            EnvironmentID    INTEGER NOT NULL,
            Name        TEXT    NOT NULL,
            Image       TEXT    NOT NULL,
            PRIMARY KEY (EnvironmentID)
        )
        '''

        animals_sql = '''
        CREATE TABLE IF NOT EXISTS Albums (
            EnvironmentID    INTEGER NOT NULL,
            AnimalID     INTEGER NOT NULL,
            Name        TEXT    NOT NULL,
            Image       TEXT    NOT NULL,
            PRIMARY KEY (AnimalID),
            FOREIGN KEY (EnvironmentID) REFERENCES Artists(EnvironmentID)
        )
        '''

        track_sql = '''
        CREATE TABLE IF NOT EXISTS Tracks (
            AnimalID     INTEGER NOT NULL,
            TrackID     INTEGER NOT NULL,
            Number      INTEGER NOT NULL,
            Name        TEXT NOT NULL,
            PRIMARY KEY (TrackID),
            FOREIGN KEY (AnimalID) REFERENCES Animals(AnimalID)
        )
        '''

        with self.conn:
            cursor = self.conn.cursor()
            cursor.execute(environments_sql)
            cursor.execute(animals_sql)
            cursor.execute(track_sql)

    def _load_tables(self):
        environment_sql = 'INSERT OR REPLACE INTO Environments (EnvironmentId, Name, Image) VALUES (?, ?, ?)'
        animal_sql  = 'INSERT OR REPLACE INTO Albums  (EnvironmentID, AnimalID, Name, Image) VALUES (?, ?, ?, ?)'
        track_sql  = 'INSERT OR REPLACE INTO Tracks  (AlbumID, TrackID, Number, Name) VALUES (?, ?, ?, ?)'

        with self.conn:
            cursor    = self.conn.cursor()
            environment_id = 1
            animal_id  = 1
            track_id  = 1

            for environment in yaml.load(open(self.data)):
                cursor.execute(environment_sql, (environment_id, environment['name'], environment['image']))
                self.logger.debug('Added Environment: id={}, name={}'.format(environment_id, environment['name']))

                for animal in environment['animal']:
                    cursor.execute(album_sql, (environment_id, animal_id, animal['name'], animal['image']))
                    self.logger.debug('Added Animal: id={}, name={}'.format(animal_id, animal['name']))

                    for track_number, track_name in enumerate(animal['tracks']):
                        cursor.execute(track_sql, (animal_id, track_id, track_number + 1, track_name))
                        self.logger.debug('Added Track: id={}, name={}'.format(track_id, track_name))
                        track_id += 1

                    animal_id += 1

                environment_id += 1

    def environments(self, environment):
        environments_sql = 'SELECT EnvironmentID, Name, Image FROM Environments WHERE Name LIKE ? ORDER BY Name'
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(environments_sql, ('%{}%'.format(environment),))

    def environment(self, environment_id=None):
        environment_sql = '''
        SELECT      AnimalID, Name, Image
        FROM        Animals
        WHERE       EnvironmentID = ?
        ORDER BY    Name
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(environment_sql, (environment_id,))

    def animals(self, album):
        albums_sql = 'SELECT AnimalID, Name, Image FROM Animals WHERE Name LIKE ? ORDER BY Name'
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(animals_sql, ('%{}%'.format(animal),))

    def animal(self, animal_id=None):
        animal_sql = '''
        SELECT      TrackID, Number, Name
        FROM        Tracks
        WHERE       AnimalId = ?
        ORDER BY    Number
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(animal_sql, (animal_id,))

    def tracks(self, track):
        tracks_sql = '''
        SELECT      Tracks.TrackID, Tracks.Name, Image
        FROM        Tracks
        JOIN        Animals
        ON          Tracks.AnimalID = Animals.AnimalID
        WHERE       Tracks.Name LIKE ?
        ORDER BY    Tracks.Name
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(tracks_sql, ('%{}%'.format(track),))

    def track(self, track_id=None):
        track_sql = '''
        SELECT      TrackID, Environments.EnvironmentID, Environments.Name, Animals.AnimalID, Animals.Name, Number, Tracks.Name, Animals.Image
        FROM        Tracks
        JOIN        Animals
        ON          Animals.AnimalID = Tracks.AnimalID
        JOIN        Environments
        ON          Environments.EnvironmentID = Animals.EnvironmentID
        WHERE       TrackID = ?
        '''
        with self.conn:
            cursor = self.conn.cursor()
            return cursor.execute(track_sql, (track_id,)).fetchone()

    def song(self, track_id=None):
        # TODO: Select specific song information
    trackinfo=self.track(track_id)
    if trackinfo:
        trackdict={
        'trackName': trackinfo[6],
        'environmenttName': trackinfo[2],
        'animalName': trackinfo[4],
        'animalImage': trackinfo[7],
        'trackNumber': trackinfo[5],
        'songURL': "/assets/mp3/{:04d}.mp3".format(int(track_id))
        }
    return trackdict

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
