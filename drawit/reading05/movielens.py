#!/usr/bin/env python

import logging
import os
import random
import sqlite3
import sys

import tornado.ioloop
import tornado.options
import tornado.web

# Constants

PORT      = 9999
DB_PATH   = 'movielens.db'
DATA_PATH = 'movies.dat'

# Database

class Database(object):

    def __init__(self, path=DB_PATH):
        # TODO 1: Connect to database, create tables, and load tables
        self.logger = logging.getLogger()
        self.path   = path
        self.conn   = sqlite3.connect(DB_PATH)

        self._create_tables()
        self._load_tables()

    def _create_tables(self):
        # TODO 2: Complete movies_sql and genres_sql
        with self.conn:
            cursor = self.conn.cursor()
            movies_sql = '''
            CREATE Table IF NOT EXISTS Movies{
                MovieID     INTEGER     NOT NULL,
                Title       TEXT        NOT NULL,
                Year        INTEGER     NOT NULL
                PRIMARY KEY(MovieID)
            }
            '''
            genres_sql = '''
            CREATE Table IF NOT EXISTS Genres{
                MovieID     INTEGER     NOT NULL,
                Genre       TEXT        NOT NULL,
                PRIMARY KEY(MovieID, Genre),
                FOREIGN KEY(MovieID) References Movies(MovieID)
            }
            '''

            cursor.execute(movies_sql)
            self.logger.info('Created Movies Table')

            cursor.execute(genres_sql)
            self.logger.info('Created Genres Table')

    def _load_tables(self):
        # TODO 3: Complete movie_sql and genre_sql
        with self.conn:
            cursor    = self.conn.cursor()
            movie_sql = '''
            INSERT OR REPLACE INTO Movies (MovieID, Title, Year)
            VALUES (?, ?, ?)
            '''
            genre_sql = '''
            INSERT OR REPLACE INTO Genres (MovieID, Genre)
            VALUES (?, ?)
            '''

            for line in open(DATA_PATH):
                movie_id, title, genres = line.strip().decode('utf-8', 'ignore').split('::')
                year      = int(title[-5:-1])
                title     = title[:-7]
                genres    = genres.split('|')

                cursor.execute(movie_sql, (movie_id, title, year))
                self.logger.info('Inserted Movie: MovieID={}, Title={}, Year={}'.format(movie_id, title, year))

                for genre in genres:
                    cursor.execute(genre_sql, (movie_id, genre))
                    self.logger.info('Inserted Genre: MovieID={}, Genre={}'.format(movie_id, genre))

    def movies(self, title, limit):
        # TODO 4: Complete movie_sql
        with self.conn:
            cursor    = self.conn.cursor()
            movie_sql = '''
            SELECT Movies. MovieID, Title, Year, GROUP_CONCAT(Genres.Genre) AS Genres
            FROM Movies
            JOIN Genres ON Movies.MovieID=Genres.MovieID
            WHERE Title LIKE (?)
            GROUP BY Movies.MovieID
            LIKE (?)
            '''
            return cursor.execute(movie_sql, (title, limit))

    def genre(self, genre):
        # TODO 5: Complete genre_sql
        with self.conn:
            cursor    = self.conn.cursor()
            genre_sql = '''
            SELECT Movies. MovieID, Title, Year, GROUP_CONCAT(Genres.Genre) AS Genres
            FROM Movies
            JOIN Genres ON Movies.MovieID=Genres.MovieID
            WHERE Genre LIKE (?)
            GROUP BY Movies.MovieID
            '''
            return cursor.execute(genre_sql, (genre,))

    def year(self, year):
        # TODO 6: Complete year_sql
        with self.conn:
            cursor   = self.conn.cursor()
            year_sql = '''
            SELECT Movies. MovieID, Title, Year, GROUP_CONCAT(Genres.Genre) AS Genres
            FROM Movies
            JOIN Genres ON Movies.MovieID=Genres.MovieID
            WHERE Year LIKE (?)
            GROUP BY Movies.MovieID
            '''
            return cursor.execute(year_sql, (year,))

    def genres(self):
        # TODO 7: Complete genres_sql
        with self.conn:
            cursor     = self.conn.cursor()
            genres_sql = '''
            SELECT DISTINCT     "Genre"
            FROM Genres
            GROUP BY Genres.MovieID
            '''
            return cursor.execute(genres_sql)

    def years(self):
        # TODO 8: Complete years_sql
        with self.conn:
            cursor     = self.conn.cursor()
            years_sql  = '''
            SELECT DISTINCT     "Year"
            FROM Movies
            GROUP BY Movies.MovieID
            '''
            return cursor.execute(years_sql)

# Handlers

class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        title  = self.get_argument('title', '')
        limit  = int(self.get_argument('limit', 0))
        movies = self.application.database.movies('%{}%'.format(title), limit)

        self.render('movies.html', title=title, movies=movies)

class GenreHandler(tornado.web.RequestHandler):
    def get(self, genre=None):
        if genre:
            movies = self.application.database.genre(genre)
            self.render('movies.html', title='', movies=movies)
        else:
            groups = self.application.database.genres()
            self.render('groups.html', title='', groups=groups, prefix='genre')

class YearHandler(tornado.web.RequestHandler):
    def get(self, year=None):
        if year:
            movies = self.application.database.year(year)
            self.render('movies.html', title='', movies=movies)
        else:
            groups = self.application.database.years()
            self.render('groups.html', title='', groups=groups, prefix='year')

# Application

class Application(tornado.web.Application):

    def __init__(self, port=PORT):
        tornado.web.Application.__init__(self, debug=True)
        self.logger   = logging.getLogger()
        self.database = Database()
        self.ioloop   = tornado.ioloop.IOLoop.instance()
        self.port     = port

        self.add_handlers('', [
            (r'/'          , IndexHandler),
            (r'/genre/(.*)', GenreHandler),
            (r'/year/(.*)' , YearHandler),
        ])

    def run(self):
        try:
            self.listen(self.port)
        except socket.error as e:
            self.logger.fatal('Unable to listen on {} = {}'.format(self.port, e))
            sys.exit(1)

        self.ioloop.start()

# Main Execution

if __name__ == '__main__':
    tornado.options.parse_command_line()

    application = Application()
    application.run()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
