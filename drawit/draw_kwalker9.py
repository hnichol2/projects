from drawit import Image, Color, Point, PPM

import math
import random

#Colors for image
WHITE     = Color(255, 255, 255)
BLACK     = Color(0, 0, 0)
BLUE	  = Color(0, 204, 204)
GREY	  = Color(160, 160, 160)
DARK_GREY = Color(128, 128, 128)
RED		  = Color(255, 0, 0)
GOLD	  = Color(189, 189, 102)

class Robot(Image):

    def draw_background(self):
        self.draw_rectangle(Point(0, 0), Point(self.width-1, self.height-1), BLUE)

    def draw_robot(self):
    	# light
    	self.draw_circle(Point(float(319.5), 180), 15, RED)
    	# antenna
    	self.draw_line(Point(float(319.5), 150), Point(float(319.5),165), BLACK)
    	self.draw_circle(Point(float(319.5), 150), 5, BLACK)
    	# body
    	self.draw_rectangle(Point(269, 249), Point(370, 409), GREY)
    	# leg 1
    	self.draw_rectangle(Point(282, 409), Point(304, 469), GREY)
    	# leg 2
    	self.draw_rectangle(Point(335, 409), Point(357, 469), GREY)
    	# foot 1
    	self.draw_rectangle(Point(279, 469), Point(307, 479), BLACK)
    	# foot 2
    	self.draw_rectangle(Point(332, 469), Point(360, 479), BLACK)
    	# head
    	self.draw_rectangle(Point(279, 176), Point(360, 242), GREY)
    	# eye 1
    	self.draw_circle(Point(float(300), 200), 12, WHITE)
    	self.draw_circle(Point(float(300), 200), 6, BLACK)
    	# eye 2
    	self.draw_circle(Point(float(339), 200), 12, WHITE)
    	self.draw_circle(Point(float(339), 200), 6, BLACK)
    	#neck
    	self.draw_rectangle(Point(304, 242), Point(335, 249), BLACK)
    	#shoulder 1
    	self.draw_rectangle(Point(244, 259), Point(269, 284), DARK_GREY)
    	#shoulder 2
    	self.draw_rectangle(Point(370, 259), Point(395, 284), DARK_GREY)
    	#arm 1
    	self.draw_rectangle(Point(247, 284), Point(266, 344), GREY)
    	#arm 2
    	self.draw_rectangle(Point(373, 284), Point(392, 344), GREY)
    	#mouth
    	self.draw_rectangle(Point(300, 222), Point(339, 224), BLACK)
    	#mid-line
    	self.draw_line(Point(269, 359), Point(370, 359), DARK_GREY)
    	# hand 1
    	self.draw_rectangle(Point(245, 344), Point(266, 364), GOLD)
    	# hand 2
    	self.draw_rectangle(Point(372, 344), Point(393, 364), GOLD)

        
if __name__ == '__main__':
    #draw robot
	robot=Robot()
	robot.draw_background()
	robot.draw_robot()

    #Write image to stdout
	PPM.write(robot)