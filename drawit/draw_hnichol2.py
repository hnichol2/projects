#! /usr/bin/env python
from drawit import Image, Color, Point, PPM
import math
import random

# netid colors 
WHITE    = Color(255, 255, 255)
BLACK    = Color(0, 0, 0)
PINK     = Color(245, 76, 239)
GREEN    = Color(39, 245, 70)
YELLOW   = Color(247, 244, 17)

class NETID(Image):
    def draw_background(self):
        self.draw_rectangle(Point(0, 0), Point(self.width-1, self.height-1), YELLOW)
    def draw_netid(self):
        #H
        self.draw_rectangle(Point(110, 360), Point(130, 120), PINK) #1
        self.draw_rectangle(Point(200, 360), Point(220, 120), PINK) #2
        self.draw_rectangle(Point(110, 260), Point(220, 220), PINK) #3

        #N
        self.draw_rectangle(Point(340, 360), Point(360, 120), GREEN) #1
        self.draw_line(Point(360, 120), Point(490, 360), GREEN) #2
        self.draw_line(Point(359, 120), Point(489, 360), GREEN)
        self.draw_line(Point(358, 120), Point(488, 360), GREEN)
        self.draw_line(Point(357, 120), Point(487, 360), GREEN)
        self.draw_line(Point(356, 120), Point(486, 360), GREEN)
        self.draw_line(Point(355, 120), Point(485, 360), GREEN)
        self.draw_line(Point(354, 120), Point(484, 360), GREEN)
        self.draw_line(Point(353, 120), Point(483, 360), GREEN)
        self.draw_line(Point(352, 120), Point(482, 360), GREEN)
        self.draw_line(Point(351, 120), Point(481, 360), GREEN)
        self.draw_line(Point(350, 120), Point(480, 360), GREEN)
        self.draw_line(Point(349, 120), Point(479, 360), GREEN)
        self.draw_line(Point(348, 120), Point(478, 360), GREEN)
        self.draw_line(Point(347, 120), Point(477, 360), GREEN)
        self.draw_line(Point(346, 120), Point(476, 360), GREEN)
        self.draw_line(Point(345, 120), Point(475, 360), GREEN)
        self.draw_line(Point(344, 120), Point(474, 360), GREEN)
        self.draw_line(Point(343, 120), Point(473, 360), GREEN)
        self.draw_line(Point(342, 120), Point(472, 360), GREEN)
        self.draw_line(Point(341, 120), Point(471, 360), GREEN)
        self.draw_line(Point(340, 120), Point(470, 360), GREEN)
        self.draw_rectangle(Point(470, 360), Point(490, 120), GREEN) #3


if __name__ == '__main__':
    #draw netid
    ni=NETID()
    ni.draw_background()
    ni.draw_netid()

    #Write image to stdout
    PPM.write(ni)