Project 01 - Grading
====================

**Group Score**: 26.5 / 27

**bchong Score**:  3 / 3

**hnichol2 Score**:  3 / 3

**kwalker9 Score**:  3 / 3

Deductions
----------

1.) Color: __eq__(): -0.5

Comments
--------
Great job! You passed all the tests. Keep up the good work!

1.) WARNING: in your __eq__() method of your Color class, you do an if check for when your color values are the same. However, you do not return explicitly False if the 'if statment' is not True. I noticed you placed "self.r==other.r and self.g==other.g and self.b==other.b" as the return value for the True case. Instead of doing an if/else case you could have simply just returned "self.r==other.r and self.g==other.g and self.b==other.b". This is correct because the return value with either be True or False value.

NOTE: for your draw_line method of your Image class, you could have simply used the max and min methods to calculate the maximum/minimum x and y points.
