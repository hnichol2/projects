Reading 09
==========

## Responses

1. We plan on making a website that can play different animal sounds. We plan on making the website similar to our PlayIt project, but reorganize the tabs and overall look to fit our animal theme. We want to organize our data in categories such as "birds" and "mammals" with different sounds and animals in each category, or make categories like "jungle" and "ocean" and add animals and sounds that are heard in that environment. 

2. Our project meets the requirements above because it will use a website using Bootstrap and Tornado, and JavaScript will be used to make the functions to run the website. SQLite will also be used because we will make a database for our animals. AJAX and JSON will also be used to run the website and get the correct data. 

3. We have not decided specific roles for each person yet, but we think we will probably split the work by functions so that we don't work on the same things at the same time, as that was what we have been doing for our past projects. 

4. We need to address how we are going to organize our data and how we are going to get the sounds and information needed to make our website. 


## Feedback

## Grading

